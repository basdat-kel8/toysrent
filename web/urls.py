from django.urls import path
from django.conf.urls import *
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('buatpengiriman/', views.buatpengiriman, name ='buatpengiriman'),
    path('pengiriman/', views.pengiriman, name ='pengiriman'),
    path('review/', views.review, name='review'),
    path('buatreview/', views.buatreview, name='buatreview'),
    path('chat/', views.chat, name ='chat'),
    path('daftarbarang/', views.daftar_barang, name ='daftar_barang'),
    path('detailbarang/', views.detail_barang, name='detail_barang'),
    path('ubahbarang/', views.ubah_barang, name='ubah_barang'),
    path('registadmin/', views.regist_admin, name='regist_admin'),
    path('add_regist_admin/',views.add_regist_admin,name='add_regist_admin'),
    path('registanggota/', views.regist_anggota, name='regist_anggota'),
    path('profile/', views.profile, name='profile'),
    path('item/', views.item, name='item'),
    path('item_tambah/', views.item_tambah, name='item_tambah'),
    path('item_update/', views.item_update, name='item_update'),
    path('pesanan/', views.pesanan, name='pesanan'),
    path('buatpesanan/', views.buatpesanan, name='buatpesanan'),
    path('updatepesanan/', views.updatepesanan, name='updatepesanan'),
    path('level/', views.level, name='level'),
    path('buatlevel/', views.buatlevel, name='buatlevel'),
    path('updatelevel/', views.buatlevel, name='updatelevel'),
]
