from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection
from .forms import *

import psycopg2
from pprint import pprint

class DBConnect:
    try:
        def __init__(self):
        self.connection = psycopg2.connect("dbname='d81t74eocfcacj' user='lmnfplozltlgvm' password='a83cf7fdd5732366a4c5f00f124c102bc954dd77526a710306e14595da780d54'")
        self.connection.autocommit = True
        self.cursor1 = self.connection.cursor()
    except:
        pprint("Can't connect to the database")

    def insert_data_pengiriman(self, arg_input_data):
        input_no_resi = arg_input_data['input_no_resi']
        input_id_pemesanan = arg_input_data['input_id_pemesanan']
        input_metode = arg_input_data['input_metode']
        input_ongkos = arg_input_data['input_ongkos']
        input_tanggal = arg_input_data['input_tanggal']
        input_no_ktp_anggota = arg_input_data['input_no_ktp_anggota']
        input_nama_alamat_anggota = arg_input_data['input_nama_alamat_anggota']
        insert_command = ("INSERT INTO TOYSRENT.PENGIRIMAN(no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota)"
            +"VALUES('"+input_no_resi+"', '"+input_id_pemesanan+"', '"+input_metode+"', '"+input_ongkos+"', '"+input_tanggal
            +"', '"+input_no_ktp_anggota+"', '"+input_nama_alamat_anggota+"')")
        pprint(insert_command)
        self.cursor1.execute(insert_command)

    def insert_data_review(self, arg_input_data):
        input_no_resi = arg_input_data['input_no_resi']
        input_no_urut = arg_input_data['input_no_urut']
        input_id_barang = arg_input_data['input_id_barang']
        input_tanggal_review = arg_input_data['input_tanggal_review']
        input_review = arg_input_data['input_review']
        insert_command = ("INSERT INTO TOYSRENT.BARANG_DIKIRIM(no_resi, no_urut, id_barang, tanggal_review, review)"
            +"VALUES('"+input_no_resi+"', '"+input_no_urut+"', '"+input_id_barang+"', '"+input_tanggal_review+"', '"+input_review+"')")
        pprint(insert_command)
        self.cursor1.execute(insert_command)

    def insert_data_pesanan(self, arg_input_data):
		input_id_pemesanan = arg_input_data['input_id_barang']
		input_datetime_pesanan = arg_input_data['input_datetime_pesanan']
		input_kuantitas_barang = arg_input_data['input_kuantitas_barang']
		input_harga_sewa = arg_input_data['input_harga_sewa']
		input_ongkos = arg_input_data['input_ongkos']
		input_no_ktp_pemesan = arg_input_data['input_no_ktp_pemesan']
		input_status = arg_input_data['input_status']
		insert_command = ("INSERT INTO TOYSRENT.PEMESANAN(id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status)"
            +"VALUES('"+input_id_pemesanan+"', '"+input_datetime_pesanan+"', '"+input_kuantitas_barang+"', '"+input_harga_sewa+"', '"+input_ongkos+"', '"+input_no_ktp_pemesan+"', '"+input_status+"')")
        pprint(insert_command)
        self.cursor1.execute(insert_command)
		
	def insert_def_level(self, arg_input_data:
		input_nama_level = arg_input_data['input_nama_level']
		input_minimum_poin = arg_input_data['input_minimum_poin']
		input_deskripsi = arg_input_data['input_deskripsi']
		insert_command = ("INSERT INTO TOYSRENT.LEVEL_KEANGGOTAAN(nama_level, minimum_poin, deskripsi)"
            +"VALUES('"+input_nama_level+"', '"+input_minimum_poin+"', '"+input_deskripsi+"')")
        pprint(insert_command)
        self.cursor1.execute(insert_command)


response = {}

def index(request):
    return render(request, 'web/login.html')

def buatpengiriman(request):
    input_data = {}
    form = PengirimanForm(request.POST or None)
    if (request.method = 'POST' and form.is_valid()):
        input_data['input_no_resi'] = request.POST.get('no_resi')
        input_data['input_id_pemesanan'] = request.POST.get('id_pemesanan')
        input_data['input_metode'] = request.POST.get('metode')
        input_data['input_ongkos'] = request.POST.get('ongkos')
        input_data['input_tanggal'] = request.POST.get('tanggal')
        input_data['input_no_ktp_anggota'] = request.POST.get('no_ktp_anggota')
        input_data['input_nama_alamat_anggota'] = request.POST.get('nama_alamat_anggota')
        
        db_insert = DBConnect()
        db_insert.insert_data_registrant(input_data)

    else:
        response['form_pengiriman'] = PengirimanForm()
    return render(request, 'web/buatpengiriman.html', response)

def pengiriman(request):

    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to ToysRent')
    cursor.execute('select no_resi,id_pemesanan,ongkos,tanggal from pengiriman')

    hasil1 = cursor.fetchall()
    response['pengiriman'] = hasil1

    return render(request, 'web/pengiriman.html', response)

def review(request):

    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to ToysRent')
    cursor.execute('select no_resi,id_barang,review,tanggal_review from barang_dikirim')

    hasil1 = cursor.fetchall()
    response['review'] = hasil1

    return render(request, 'web/review.html')

def buatreview(request):
    input_data = {}
    form = PengirimanForm(request.POST or None)
    if (request.method = 'POST' and form.is_valid()):
        input_data['input_no_resi'] = request.POST.get('no_resi')
        input_data['input_no_urut'] = request.POST.get('no_urut')
        input_data['input_id_barang'] = request.POST.get('id_barang')
        input_data['input_tanggal_review'] = request.POST.get('tanggal_review')
        input_data['review'] = request.POST.get('review')
        
        db_insert = DBConnect()
        db_insert.insert_data_registrant(input_data)

    else:
        response['form_review'] = ReviewForm()
    return render(request, 'web/buatreview.html', response)
    
def chat(request):
    return render(request, 'web/chat.html')

def daftar_barang(request):
    return render(request, 'web/daftar_barang.html')

def detail_barang(request):
    return render(request, 'web/Detail_barang.html')

def ubah_barang(request):
    return render(request, 'web/ubah_barang.html')

def regist_admin(request):
    return render(request, 'web/regist_admin.html')

def add_regist_admin(request):
    print('masuk1')
    input_data = {}
    if request.method == 'POST':
        form = RegistForm(request.POST)
        if form.is_valid():
            input_data['input_KTP'] = request.POST['ktp']
            input_data['input_nama'] = request.POST['nama']
            input_data['input_email'] = request.POST['email']
            input_data['input_tgl_lahir'] = request.POST['tgl_lahir']
            input_data['input_alamat'] = request.POST['alamat']
            input_data['input_no_telp'] = request.POST['no_telp']

            db_insert = DBConnect()
            db_insert.insert_data_registrant(input_data)
    else:
        print('masuk2')
        form = RegistForm()
    response['form_register'] = RegistForm()
    # response = {'form_register': form}
    return render(request, 'regist_admin.html', response)

def regist_anggota(request):
    return render(request, 'web/regist_anggota.html')

def profile(request):
    return render(request, 'web/profil.html')

def item_tambah(request):
    return render(request, 'web/item_tambah.html')

def item(request):
    return render(request, 'web/item.html')

def list_item(request):
    reponse={}
    cursor = connection.cursor()
    cursor.execute('set search_path to ToysRent')
    cursor.execute("select * from item;")

    hasil1 = cursor.fetchall()
    response['item'] = hasil1

    return render(request, 'web/item.html', response)

def item_update(request):
    return render(request, 'web/item_update.html')

def buatpesanan(request):
	input_data = {}
	form = PesananForm(request.POST or None)
	if(request.method = 'POST' and form.is_valid()):
		input_data['input_id_pemesanan'] = request.POST.get('id_pemesanan')
		input_data['input_datetime_pesanan'] = request.POST.get('datetime_pesanan')
		input_data['input_kuantitas_barang'] = request.POST.get('kuantitas_barang')
		input_data['input_harga_sewa'] = request.POST.get('harga_sewa')
		input_data['input_ongkos'] = request.POST.get('ongkos')
		input_data['input_no_ktp_pemesan'] = request.POST.get('no_ktp_pemesan')
		input_data['input_status'] = request.POST.get('status')
		
		db_insert = DBConnect()
		db_insert.insert_data_registrant(input_data)
	
	else:
		response['form_pesanan'] = PesananForm()
    return render(request, 'web/buatpesanan.html', response)
	
def pesanan(request):
	response = {}
	cursor = connection.cursor()
	cursor.execute('set search_path to ToysRent')
	cursor.execute('select id_pemesanan,datetime_pesanan,harga_sewa,status')
	
	hasil1 = cursor.fetchall()
	response['pesanan'] = hasil1
	return render(request, 'web/pesanan.html', response)

def updatepesanan(request):
    return render(request, 'web/updatepesanan.html')

def level(request):
    return render(request, 'web/Level.html')

def buatlevel(request):
	input_data = {}
	form = LevelForm(request.POST or None)
	if(request.method = 'POST' and form.is_valid()):
		input_data['input_nama_level'] = request.POST.get('nama_level')
		input_data['input_minimum_poin'] = request.POST.get('minimum_poin')
		input_data['input_deskripsi'] = request.POST.get('deskripsi')
		
		db_insert = DBConnect()
		db_insert.insert_data_registrant(input_data)
	
	else:
		response['form_level'] = LevelForm()
    return render(request, 'web/buatlevel.html', response)

def updatelevel(request):
    return render(request, 'web/updatelevel.html')


