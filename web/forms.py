from django import forms

fieldStyling = (
	'margin-left: auto;'
	+'margin-right: auto;'
	+'margin-top: 10px;'
	+'margin-bottom: 10px;'
	+'padding-left: 25px;'
	+'padding-right: 25px;'
	+'padding-top: 5px;'
	+'padding-top: 5px;'
	+'min-width: 260px;'
	+'justify-content: center;'
)

class RegistForm(forms.Form):
    ktp = forms.IntegerField(label="",
        required = True,
        # max_length = 20,
        widget= forms.TextInput(attrs={
            'class':'form-control',
            'id': 'regist_name',
            'type': 'number',
            'placeholder':'Nomor KTP'})
        )

    email = forms.EmailField(label="",
        required = True,
        # max_length = 50,
        widget= forms.TextInput(attrs={
            'class':'form-control',
            'id': 'regist_email',
            'type': 'email',
            'placeholder':'E-mail'})
        )
    
    nama = forms.CharField(label="",
        required = True,
        # max_length = 50,
        widget= forms.TextInput(attrs={
            'class':'form-control',
            'id': 'regist_name',
            'type': 'text',
            'placeholder':'Nama Anda'})
        )

    tgl_lahir = forms.DateField(label="",
        required = True,
        widget= forms.DateInput(
            format='%m/%d/%Y',
            attrs={
            'class':'form-control',
            'id': 'regist_birthdate',
            'type': 'date',
            'placeholder':'Tanggal Lahir'}),
        input_formats=('%Y-%m-%d', )
        )

    alamat = forms.CharField(label="",
        required = True,
        widget= forms.TextInput(attrs={
            'class':'form-control',
            'id': 'regist_alamat',
            'type': 'text',
            'placeholder':'Alamat'})
        )

    no_telp = forms.IntegerField(label="",
        required = True,
        # max_length = 20,
        widget= forms.TextInput(attrs={
            'class':'form-control',
            'id': 'regist_phone',
            'type': 'number',
            'placeholder':'Nomor Telepon'})
			
class PengirimanForm(forms.Form):
	no_resi = forms.CharField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'no_resi', 
					'type': 'text',
					'style': fieldStyling })
		)
	
	id_pemesanan = forms.CharField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'id_pemesanan', 
					'type': 'text',
					'style': fieldStyling })
		)

	metode = forms.CharField(label='',
		#max_length=128,
		required=False,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'metode', 
					'type': 'text', 
					'style': fieldStyling })
		)

	ongkos = forms.IntegerField(label='',
		#max_length=128,
		required=False,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'ongkos', 
					'type': 'number', 
					'style': fieldStyling })
		)

	tanggal = forms.DateField(label='',
		required=True, 
		#input_formats=settings.DATE_INPUT_FORMATS,
		widget=forms.DateInput(
					format='%m/%d/%Y',
					attrs={
					'class': 'form-control',
					'id': 'tanggal',
					'type': 'date',
					'style': fieldStyling }),
		input_formats=('%Y-%m-%d', )
		)

	no_ktp_anggota = forms.IntegerField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'no_ktp_anggota', 
					'type': 'number',
					'style': fieldStyling })
		)

	nama_alamat_anggota = forms.CharField(label='',
		required=True,
		#max_length=128,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'nama_alamat_anggota', 
					'type':'text',
					'style': fieldStyling })
		)
	
class ReviewForm(forms.Form):
	no_resi = forms.CharField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'no_resi', 
					'type': 'text',
					'style': fieldStyling })
		)

	no_urut = forms.CharField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'no_urut', 
					'type': 'text',
					'style': fieldStyling })
		)

	id_barang = forms.CharField(label='',
		required=True,
		#max_length=32,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'id_barang', 
					'type': 'text',
					'style': fieldStyling })
		)

	tanggal_review = forms.DateField(label='',
		required=True, 
		#input_formats=settings.DATE_INPUT_FORMATS,
		widget=forms.DateInput(
					format='%m/%d/%Y',
					attrs={
					'class': 'form-control',
					'id': 'tanggal_review',
					'type': 'date',
					'style': fieldStyling }),
		input_formats=('%Y-%m-%d', )
		)
	
	review = forms.CharField(label='',
		required=True,
		#max_length=128,
		widget=forms.TextInput(attrs={
					'class': 'form-control',
					'id': 'review', 
					'type':'text',
					'style': fieldStyling })
		)

class PesananForm(forms.Form):
	id_pemesanan = forms.CharField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'id_pemesanan',
				'type': 'text',
				'style': fieldStyling})
		)
		
	datetime_pesanan = forms.CharField(label='',
		required = True,
		#max_length = 32,
		widget = forms.DateInput(
				format='%m/%d/%Y',
				attrs={
				'class': 'form-control',
				'id': 'datetime_pesanan',
				'type': 'date',
				'style': fieldStyling}),
			input_formats = ('%Y-%m-%d')
		)
	
	kuantitas_barang = forms.IntegerField(label='',
		required = False,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'kuantitas_barang',
				'type': 'number',
				'style': fieldStyling})
		)
	
	harga_sewa = forms.IntegerField(label='',
		required = False,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'harga_sewa',
				'type': 'number',
				'style': fieldStyling})
		)
	
	ongkos = forms.IntegerField(label='',
		required = False,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'ongkos',
				'type': 'number',
				'style': fieldStyling})
		)
	
	no_ktp_pemesan = forms.IntegerField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'no_ktp_pemesan',
				'type': 'number',
				'style': fieldStyling})
		)
	
	status = forms.CharField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'status',
				'type': 'text',
				'style': fieldStyling})
		)
	
class LevelForm(forms.Form):
	nama_level = forms.CharField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'nama_level',
				'type': 'text',
				'style': fieldStyling})
		)
	
	minimum_poin = forms.IntegerField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'minimum_poin',
				'type': 'number',
				'style': fieldStyling})
		)
	
	deskripsi = forms.CharField(label='',
		required = True,
		#max_length = 32,
		widget = forms.TextInput(attrs={
				'class': 'form-control',
				'id': 'deskripsi',
				'type': 'text',
				'style': fieldStyling})