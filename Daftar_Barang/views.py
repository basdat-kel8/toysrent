from django.shortcuts import render
from django.db import connection

# Create your views here.

def listbarang(request):
    
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to ToysRent')
    cursor.execute("select * from barang;")

    hasil1 = cursor.fetchall()
    response['barang'] = hasil1
   

 
    return render(request, 'listbarang.html',response)
